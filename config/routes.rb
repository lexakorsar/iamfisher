Rails.application.routes.draw do

  root 'main#index'

  devise_for :admins

  devise_for :users,
             controllers:
                 {
                     sessions: 'users/sessions',
                     confirmations: 'users/confirmations',
                     registrations: 'users/registrations',
                     passwords: 'users/passwords',
                     unlocks: 'users/unlocks',
                     omniauth_callbacks: 'omniauth_callbacks'
                 }

  scope module: :v1, constraints: ApiVersion.new('v1', true) do
    resources :locations
  end


  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  mount Ckeditor::Engine => '/ckeditor'
  # mount Thredded::Engine => '/forum'
  mount Commontator::Engine => '/commontator'

  namespace :users do
    root 'profile#index'
    get 'profile/:id' => 'profile#edit', :as => :user_edit
    patch 'profile/update' => 'profile#update', :as => :user_update
    patch '/blog'=> 'blog#create'

    get '/create_product' => 'profile#new_product', :as => :create_product
    get '/edit_product/:id' => 'profile#edit_product', :as => :edit_product
    get '/product/:id' => 'profile#show_product', :as => :show_product
    post '/save_product' => 'profile#create_product', :as => :save_product
    patch '/update_product' => 'profile#update_product', :as => :update_product
    delete 'delete_product' => 'profile#delete_product', :as => :delete_product

    resources :blog do
      post 'photos'=> 'blog#photo_upload'

      patch 'photos'=> 'blog#photo_upload'
    end

  end

  get '/messages' => redirect('/conversations')
  resources :messages do
    member do
      post :new
    end
  end
  resources :conversations do
    member do
      post :reply
      post :trash
      post :untrash
    end
    collection do
      get :trashbin
      post :empty_trash
    end
  end
  # match '/users/:id/finish_signup' => 'users#finish_signup', via: [:get, :patch], :as => :finish_signup


  get 'v1/location_json' => 'locations#location_json'
  get 'v1/location_image_json/:id' => 'locations#location_image_json'
  get 'getRegion' => 'locations#getRegion', as: :get_region

  get 'private_police'=> 'base#private_police', as: :private_police
  get 'term_of_use'=> 'base#term_uses', as: :term_of_use
  get 'search'=> 'locations#index', as: :search
  get 'map'=> 'map#index', as: :map
  get 'blog'=> 'blog#index', as: :blog
  get 'contacts'=> 'locations#contacts', as: :contacts
  get 'add_location'=> 'locations#new', as: :add_location
  resources :events
  # get 'events'=> 'events#index', as: :events
  # get 'events/:id'=> 'events#show', as: :events
  get 'get_events' => 'events#get_events'
  get '/404' => 'errors#not_found'
  get '/500' => 'errors#internal_server_error'


  get  'users-list' => 'users#index', :as => :users_list
  get 'company-list' => 'users#company_index', :as => :company_list

  get 'users-list/:id' => 'users#show', :as => :user_profile

  get 'blog/:id'=> 'blog#show', as: :article
  get 'about' => 'main#about', as: :about
  get 'recommendations' => 'main#recommendations', as: :recommendations

  post 'photos'=> 'locations#photo_upload'
  patch 'locations'=> 'locations#create'
  patch 'photos'=> 'locations#photo_upload'

  get 'update_regions' => 'base#update_regions', as: 'update_regions'

  get 'sitemap.xml' => 'main#sitemap', format: :xml, as: :sitemap
  get 'robots.txt' => 'main#robots', format: :text, as: :robots

  resources :locations do

    resources :blog
  end

  resources :regions do
    resources :locations do
      resources :blog
    end
  end

  resources :competitions

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
