# Change this to your host. See the readme at https://github.com/lassebunk/dynamic_sitemaps
# for examples of multiple hosts and folders.

if ( Rails.env.development? || Rails.env.test? )
  host 'localhost:3000'
else
  host 'iamfisher.ru'
end

sitemap :site do
  url root_url, last_mod: Time.now, change_freq: 'daily', priority: 1.0
  url regions_url, last_mod: Time.now, change_freq: 'daily', priority: 1.0

  Location.all.each do |location|
    if location.present? && location.region.present?
      url region_location_url(location.region.id, location.id), last_mod: location.updated_at, priority: 1.0
    end
  end

  url events_url, last_mod: Time.now, change_freq: 'daily', priority: 1.0
  Event.all.each do |event|
    if event.present?
      url event_url( event.id), last_mod: event.updated_at, priority: 1.0
    end
  end
  url map_url, last_mod: Time.now, priority: 1.0
  url blog_url, last_mod: Time.now, change_freq: 'daily', priority: 1.0

  Article.all.each do |article|
    if article.present?
      url article_url( article.id), last_mod: article.updated_at, priority: 1.0
    end
  end
  url users_list_url, last_mod: Time.now, change_freq: 'daily', priority: 1.0

  User.all.each do |user|
    if user.present?
      url user_profile_url( user.id), last_mod: user.updated_at, priority: 1.0
    end
  end

  url contacts_url, last_mod: Time.now, priority: 1.0

  url private_police_url
  url term_of_use_url
end




# You can have multiple sitemaps like the above – just make sure their names are different.

# Automatically link to all pages using the routes specified
# using "resources :pages" in config/routes.rb. This will also
# automatically set <lastmod> to the date and time in page.updated_at:
#


# For products with special sitemap name and priority, and link to comments:
#
#   sitemap_for Product.published, name: :published_products do |product|
#     url product, last_mod: product.updated_at, priority: (product.featured? ? 1.0 : 0.7)
#     url product_comments_url(product)
#   end

# If you want to generate multiple sitemaps in different folders (for example if you have
# more than one domain, you can specify a folder before the sitemap definitions:
# 
#   Site.all.each do |site|
#     folder "sitemaps/#{site.domain}"
#     host site.domain
#     
#     sitemap :site do
#       url root_url
#     end
# 
#     sitemap_for site.products.scoped
#   end

# Ping search engines after sitemap generation:
#
ping_with "http://#{host}/sitemap.xml"