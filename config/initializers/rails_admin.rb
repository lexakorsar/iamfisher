
RailsAdmin.config do |config|
  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :admin
  end
  config.current_user_method(&:current_admin)

  # config.excluded_models = %w(FishLocation)

  ## == Cancan ==
  config.authorize_with :cancan

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    history_index
    history_show
  end

  config.model FishLocation do
    visible false
  end


  config.model Ckeditor::Asset  do
    visible do
      # controller bindings is available here. Example:
      bindings[:controller].current_admin.role == :superadmin
    end
  end
  config.model Ckeditor::AttachmentFile  do
    visible do
      # controller bindings is available here. Example:
      bindings[:controller].current_admin.role == :superadmin
    end
  end
  config.model Ckeditor::Picture  do
    visible do
      # controller bindings is available here. Example:
      bindings[:controller].current_admin.role == :superadmin
    end
  end

  config.model Commontator::Comment  do
    #visible false
  end
  config.model Commontator::Subscription  do
    #visible false
  end
  config.model Commontator::Thread  do
    #visible false
  end

  config.model Mailboxer::Conversation  do
    visible false
  end
  config.model Mailboxer::Conversation::OptOut  do
    visible false
  end
  config.model Mailboxer::Message  do
    visible false
  end
  config.model Mailboxer::Notification  do
    visible false
  end
  config.model Mailboxer::Receipt  do
    visible false
  end

  # config.model Thredded::Category  do
  #   visible false
  # end
  # config.model Thredded::Messageboard  do
  #   visible false
  # end
  # config.model Thredded::NotificationPreference  do
  #   visible false
  # end
  # config.model Thredded::Post  do
  #   visible false
  # end
  # config.model Thredded::PostNotification  do
  #   visible false
  # end
  # config.model Thredded::PrivateTopic  do
  #   visible false
  # end
  # config.model Thredded::PrivateUser  do
  #   visible false
  # end
  # config.model Thredded::Role  do
  #   visible false
  # end
  # config.model Thredded::Topic  do
  #   visible false
  # end
  # config.model Thredded::TopicCategory  do
  #   visible false
  # end
  # config.model Thredded::UserDetail  do
  #   visible false
  # end
  # config.model Thredded::UserTopicRead  do
  #   visible false
  # end
  # config.model Thredded::UserPreference  do
  #   visible false
  # end

  config.model Contact do
    visible false
    edit do
      field :contact_description
      field :contact_value
    end
  end

  config.model Photo do
    visible false
  end

  config.model Fish do
    list do
      field :name
      field :description
      field :locations
    end
  end

  config.model Region do
    list do
      field :title
      field :lat
      field :longt
      field :title

      field :description
      field :locations


    end
  end

  config.model Price do
    visible false
    edit do
      field :type_of_pryce
      field :value_of_price
    end
  end

  config.model Category do
    edit do
      field :title
      field :description
    end
  end

  # config.model User do
    # list do
    #   field :name
    #   field :soname
    #   field :email
    #   field :password
    #   field :password_confirmation
    #   field :phone
    #   field :birthday
    #   field :sex
    #   field :region
    #   field :description
    #   field :articles
    #   field :locations
    #   field :messages
    #   field :receipts
    #
    # end
  # end

  config.model EventType do
    list do
      field :name
      field :description
    end

    edit do
      field :name
      field :description, :ck_editor
    end
  end


  config.model Event do
    edit do
      field :start
      field :end
      field :title
      field :event_type
      field :description, :ck_editor
    end
  end

  config.model Article do
    edit do
      field :is_show
      field :user
      field :title
      field :logo
      field :category
      field :description, :ck_editor
      field :keywords
    end
  end

  config.model Location do
    edit do
      field :is_show
      field :title
      field :distance

      field :description, :ck_editor
      field :lat
      field :longt
      field :locations
      field :region
      field :is_payed
      field :user_price
      field :location_contact
      field :prices
      field :contacts
      field :keywords
      field :fishes
      field :photos
    end

    list do
      field :is_show
      field :distance
      field :title
      field :description
      field :lat
      field :longt
      field :locations
      field :region
      field :is_payed

      field :fishes
      field :created_at
    end
  end
end
