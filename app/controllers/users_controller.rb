class UsersController < BaseController
  # before_action :set_user
  def index
    @users = User.where(:is_company => nil)
  end

  def company_index
    @users = User.where(:is_company => true)
  end

  def show
    @user = User.find(params[:id])

    @locations = Location.where(:region_id => @user.region_id, :is_show => true)
    @user_locations = Location.where(:user_id => @user.id)
    @articles = @user.articles.where(:is_show => true).order('created_at DESC')
  end

  # GET/PATCH /users/:id/finish_signup
  # def finish_signup
  #   # authorize! :update, @user
  #     if user_params[:password].blank?
  #       user_params.delete(:password)
  #       user_params.delete(:password_confirmation)
  #     end
  #     if @user.update(user_params)
  #       # @user.skip_reconfirmation!
  #       sign_in(@user, :bypass => true)
  #       redirect_to users_root_url, notice: 'E-mail успешно обновлен.'
  #     else
  #       @show_errors = true
  #     end
  # end

  private
  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    accessible = [:email ] # extend with your own params
    accessible << [ :password, :password_confirmation ] unless params[:user][:password].blank?
    params.require(:user).permit(accessible)
  end
end
