class BaseController < ApplicationController
  layout 'application_inner'
  before_filter :get_new_messages

  def private_police

  end

  def term_uses

  end

  def get_new_messages
    if current_user.present?
      @new_messages ||= current_user.mailbox.receipts.where(is_read:false).count
    else
      @new_messages = 0
    end
  end

  def update_regions
    @regions = Region.where("province_id = ?", params[:province_id])
    respond_to do |format|
      format.js
    end
  end
end
