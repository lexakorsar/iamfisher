class MainController < ApplicationController
  def index
    @locations = Location.where(:is_show => true).last(6)
  end

  def about

  end

  def recommendations

  end

  def sitemap
    path = Rails.root.join("public", "sitemaps",  "sitemap.xml")
    if File.exists?(path)
      render xml: open(path).read
    else
      render text: "Sitemap not found.", status: :not_found
    end
  end

  def robots
  end
end
