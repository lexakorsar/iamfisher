class LocationsController < BaseController

  before_action :authenticate_user!, only: [:new, :create, :photo_upload]
  before_filter :get_location, only: [:create, :photo_upload]

  def index
    @filterrific = initialize_filterrific(
        Location,
        params[:filterrific],
        :select_options => {
            with_fish_id: Fish.options_for_select,
        }
    ) or return

    @locations = @filterrific.find

    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @location = Location.find(params[:id])
    @photos =[]

    @location.fishes

    @location.photos.each do |photo|
      @photos.push photo.image.url(:detailed)
    end
  end

  def contacts
    @locations = Location.all
  end

  def new
    if session[:location] == nil
      @location = Location.create
      # @location.photos.build
      session[:location] = @location.id
    else
      @location = Location.find(session[:location])
    end
  end

  def create
    @location.update(location_params)
    @location.user = current_user
    # @article.location = Location.find(params[:location_id])
    @location.region = Region.find(params[:location][:region])
    @location.fishes = Fish.find(params[:location][:fishes].drop(1))

    if params[:location][:price].present?
      @location.is_payed = true
    end

    # @article.category = Category.find_by_title('Отчеты')
    if @location.save
      session[:location] = nil
      redirect_to region_location_url(params[:location][:region], @location.id)
    end

  end

  def photo_upload
    # raise params.inspect
    @location.update(upload_params)

    # @photo.location = @location
    if @location.save
      @location.photos.first.is_main = true
      @location.save
      # raise @photo.inspect
      render json: { message: "success" }, :status => 200
    else
      #  you need to send an error header, otherwise Dropzone
      #  will not interpret the response as an error:
      render json: { error: @location.errors.full_messages.join(',')}, :status => 400
    end

  end

  def getRegion
    region = Region.find(params[:region])
    render json: { lat: region.lat.present? ? region.lat : 53.207112, longt: region.longt.present? ? region.longt : 45.015317 }, :status => 200
  end

  def get_location
    @location = Location.find(session[:location])
  end

  def upload_params
    params.require(:location).permit(:image, photos_attributes:[:image])
  end

  def fishes_params
    params.require(:location).permit(:fishes)
  end
  def location_params
    params.require(:location).permit(:title, :description, :location, :photo, :longt, :lat, :location_contact, :user_price)
  end

  def location_json
    @location = Location.where(:is_show => true).uniq
    # @events = Event.all
    render :json => @location.as_json
  end

  def location_image_json
    @location = Location.find(params[:id])
    render :json => @location.photos.where(:is_main => true).first.image.url(:to_main).as_json
  end

end
