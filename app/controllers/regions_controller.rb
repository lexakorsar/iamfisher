class RegionsController < BaseController
  def index
    if @province.present?
      @regions = Region.where('province_id = ?', @province.id ).options_for_select
    else
      @regions = Region.options_for_select
    end

      @filterrific = initialize_filterrific(
        Location,
        params[:filterrific],
        :select_options => {
            is_show: true,
            with_fish_id: Fish.options_for_select,
            with_region_id: @regions
        },
        default_filter_params: {:is_show => true}
    ) or return

    @locations = @filterrific.find.page(params[:page])

    respond_to do |format|
      format.html
      format.js
    end
    # @locations = Location.where(:is_show => true)
  end

  def show
    @region = Region.find(params[:id])


    @filterrific = initialize_filterrific(
        Location,
        params[:filterrific],
        :select_options => {
            is_show: true,
            with_fish_id: Fish.options_for_select,
            with_region_id: Region.options_for_select
        },
        default_filter_params: {:is_show => true}
    ) or return

    @locations = @filterrific.find.page(params[:page])

    respond_to do |format|
      format.html
      format.js
    end

  end
end
