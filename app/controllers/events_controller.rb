class EventsController < BaseController

  before_action :authenticate_user!, only: [:new, :create, :edit, :update]

  def index
  end

  def show
    @event = Event.find(params[:id])
  end

  def new
    @event = Event.new()
  end

  def create
    @event = Event.new(event_params)

    @event.user = current_user


    if @event.save
      redirect_to @event
    else
      render :action => :new
    end

  end

  def edit
    @event = Event.find(params[:id])
  end

  def update
    if @event.update(event_params)
      redirect_to @event
    else
      render :action => :edit
    end
  end

  def event_params
    params.require(:event).permit(:title, :start, :end, :description)
  end

  def get_events
    @events = Event.where(:start =>params[:start_date]..params[:end_date])
    # @events = Event.all
    respond_to do |format|
      format.json { render :json => @events.as_json }
    end
  end
end
