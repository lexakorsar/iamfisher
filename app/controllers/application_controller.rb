class ApplicationController < ActionController::Base
  before_action :configure_devise_permitted_parameters, if: :devise_controller?
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # after_filter :store_location

  before_action :set_province
  before_action { |c| current_user.track unless current_user.nil?}

  rescue_from ActiveRecord::RecordNotFound do
    flash[:warning] = 'Resource not found.'
    redirect_back_or root_path
  end

  def redirect_back_or(path)
    redirect_to request.referer || path
  end

  def set_province
    if request.subdomain.present?
      if request.subdomain != 'www'
        @province = Province.friendly.find(request.subdomain)
      end
    end

  end


  # def store_location
  #   # store last url - this is needed for post-login redirect to whatever the user last visited.
  #   return unless request.get?
  #   if (request.path != "/users/sign_in" &&
  #       request.path != "/users/sign_out" &&
  #       !request.xhr?) # don't store ajax calls
  #     session[:previous_url] = request.fullpath
  #   end
  # end

  # def after_sign_in_path_for(resource)
  #   # session[:previous_url] || root_path
  # end

  protected

  def configure_devise_permitted_parameters
    registration_params = [:name, :soname, :region_id, :email, :password, :password_confirmation, :current_password]
    registration_params2 = [:name, :soname, :region_id, :email, :password, :password_confirmation]

    if params[:action] == 'update'

      devise_parameter_sanitizer.permit(:account_update, keys: [:name, :soname, :region_id, :email, :password, :password_confirmation, :current_password])

      # devise_parameter_sanitizer.for(:account_update) {
      #     |u| u.permit(registration_params << :current_password)
      # }
    elsif params[:action] == 'create'
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :soname, :region_id, :email, :password, :password_confirmation])
      # devise_parameter_sanitizer.for(:sign_up) {
      #     |u| u.permit(registration_params)
      # }
    end
  end
end
