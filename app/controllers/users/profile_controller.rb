class Users::ProfileController < ApplicationController
  layout 'application_inner'
  before_action :authenticate_user!, :except => [:show_product]

  def index
    if params[:user_id].present?
      @user = User.find(params[:user_id])
    else
      @user = current_user
    end

    @conversationscount ||= current_user.mailbox.inbox.all
    @trash ||= current_user.mailbox.trash.all
    @locations = Location.where(:region_id => @user.region_id, :is_show => true)
    @user_locations = Location.where(:user_id => @user.id)
    @articles = @user.articles.where(:is_show => true).order('created_at DESC')

  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if user_params[:password].blank?
      user_params.delete(:password)
      user_params.delete(:password_confirmation)
    end
    if @user.update(user_params)
      sign_in(@user, :bypass => true)
      redirect_to users_root_url
    else
      render :action => :edit
    end
  end

  def show_product
    @product = Product.find(params[:id])
  end

  def new_product
    @product = Product.new()
  end

  def create_product
    @product = Product.new(product_params)

    @product.user = current_user
    if @product.save
      redirect_to users_root_url
    else
      render :action => :create_product
    end

  end

  def edit_product
    @product = Product.find(params[:id])
  end

  def update_product
    # @product = Product.new(product_params)
    @product = Product.find(params[:product][:product_id])
    if @product.update(product_params)
      redirect_to users_root_url
    else
      render :action => :edit
    end
  end

  def delete_product

  end

  def product_params
    params.require(:product).permit(:image,
                                    :name,
                                    :description,
                                    :product_category_id,
                                    :user_id,
                                    :price
    )
  end

  def user_params
    params.require(:user).permit(:avatar,
                                 :name,
                                 :soname,
                                 :phone,
                                 :description,
                                 :region_id,
                                 :birthday,
                                 :password,
                                 :email,
                                 :sex,
                                 :password_confirmation,
                                 :is_company,
                                 :payment_and_delivery,
                                 :wholesalers,
                                 :contacts,
                                 :information_for_clients,
                                 :about_company
    )
  end
end
