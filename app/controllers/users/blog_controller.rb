class Users::BlogController < BaseController

  before_action :authenticate_user!, only: [:new, :create]
  before_action :get_article, only: [:create, :photo_upload]

  def index
    @articles = Article.where(:is_show => true).order('created_at DESC')
  end

  def show
    @article = Article.find(params[:id])
  end

  def new

    if session[:article] == nil
      @article = Article.create
      # @location.photos.build
      session[:article] = @article.id
    else
      @article = Article.find(session[:article])
    end

    # @article = Article.new()
  end

  def create

    @article.update(article_params)

    @article.user = current_user
    location = Location.find(params[:article][:location])
    @article.location = location
    @article.region = location.region
    @article.category = Category.find_by_title('Отчеты')

    if @article.save
      session[:article] = nil
      redirect_to @article
    end
  end

  def photo_upload
    # raise params.inspect
    @article.update(upload_params)

    # @photo.location = @location
    if @article.save
      @article.photos.first.is_main = true
      @article.save
      # raise @photo.inspect
      render json: { message: "success" }, :status => 200
    else
      #  you need to send an error header, otherwise Dropzone
      #  will not interpret the response as an error:
      render json: { error: @article.errors.full_messages.join(',')}, :status => 400
    end

  end

  def get_article
    @article = Article.find(session[:article])
  end

  def article_params
    params.require(:article).permit(:title, :logo, :description, :region, :user)
  end

  def upload_params
    params.require(:article).permit(:image, photos_attributes:[:image])
  end
end
