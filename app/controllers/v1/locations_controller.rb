module V1
  class LocationsController < BaseController
    include Response
    include ExceptionHandler

    def index
      # get paginated current user todos
      @locations = Location.paginate(page: params[:page], per_page: 15)
      json_response(@locations)
    end

    def show
      @location = Location.find(params[:id])
      json_response(@location)
    end

  end
end