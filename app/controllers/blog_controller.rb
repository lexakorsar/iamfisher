class BlogController < BaseController

  before_action :authenticate_user!, only: [:new, :create]

  def index
    @articles = Article.page(params[:page]).where(:is_show => true).order('created_at DESC')
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new()
  end

  def create
    @article = Article.new(article_params)

    @article.user = current_user
    location = Location.find(params[:article][:location])
    @article.location = location
    @article.region = location.region
    @article.category = Category.find_by_title('Отчеты')

    @article.save
    redirect_to @article
  end

  def article_params
    params.require(:article).permit(:title, :logo, :description, :region, :user)
  end
end
