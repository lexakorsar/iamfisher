class MapController < ApplicationController
  layout 'application-map'
  def index
    if @province.present?
      @locations = Location.where(:is_show => true).where('region_id IN (?)', @province.regions.ids )
    else
      @locations = Location.where(:is_show => true)
    end

  end
end
