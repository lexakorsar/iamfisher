module ApplicationHelper

  def avatar_for(user, title = user.name)
    image_tag user.avatar.url(:small), title: title, class: 'img-rounded'
  end

  def recipients_options
    s = ''
    User.all.each do |user|
      s << "<option value='#{user.id}'>#{user.name}</option>"
    end
    s.html_safe
  end

end
