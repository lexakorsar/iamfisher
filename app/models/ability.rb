class Ability
  include CanCan::Ability
  def initialize(user)
    can :read, :all                   # allow everyone to read everything
    if user && user.admin?
      can :access, :rails_admin       # only allow admin users to access Rails Admin
      can :dashboard                  # allow access to dashboard
      if user.role? :superadmin
        can :manage, :all             # allow superadmins to do anything
      elsif user.role? :admin
        can :manage, [Location, Article, Fish, Event, EventType, User,UserType, Category, Region]
      elsif user.role? :manager
        can :manage, [Location, Article, Fish, Event]
        # can :manage, [User, Product]  # allow managers to do anything to products and users

        # can :update, Product, :hidden => false  # allow sales to only update visible products
      end
    end
  end
end
