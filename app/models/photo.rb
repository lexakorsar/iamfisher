class Photo < ActiveRecord::Base

  belongs_to :location
  belongs_to :article

  has_attached_file :image,
                    :processors => [ :watermark],
                    :styles => {
                              :detailed => {
                                  :geometry => '1920x1920>>',
                                  :watermark_path => "#{Rails.root}/public/images/logo.png",
                              },
                              :thumb => '100x100>',
                              :small  => '150x150>',
                              :medium => '200x200>',
                              :to_main => '650x350#'
                          }

  # mount :image, ImageUploader

  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  # add a delete_<asset_name> method:
  attr_accessor :delete_asset
  before_validation { self.asset.clear if self.delete_asset == '1' }
end
