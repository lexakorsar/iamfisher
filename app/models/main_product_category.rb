class MainProductCategory < ApplicationRecord
  has_many :product_categories

  def self.options_for_select
    order('LOWER(name)').map { |e| [e.name, e.id] }
  end
end
