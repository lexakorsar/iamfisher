class Province < ActiveRecord::Base
  has_many :regions
  extend FriendlyId
  friendly_id :title, use: :slugged

  def should_generate_new_friendly_id?
    new_record? || slug.blank?
  end

  def self.options_for_select
    order('LOWER(title)').map { |e| [e.title, e.id] }
  end
end
