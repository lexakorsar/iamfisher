class Location < ActiveRecord::Base
  extend WillPaginate
  include Filterable

  has_many :articles

  belongs_to :region
  belongs_to :user

  has_many :prices, :dependent => :destroy, :inverse_of => :location
  accepts_nested_attributes_for :prices, :allow_destroy => true

  has_many :contacts, :dependent => :destroy, :inverse_of => :location
  accepts_nested_attributes_for :contacts, :allow_destroy => true

  has_many :fish_locations
  has_many :fishes, through: :fish_locations

  has_many :photos, :dependent => :destroy, :inverse_of => :location
  accepts_nested_attributes_for :photos, :allow_destroy => true

  acts_as_commontable
  acts_as_votable

  # attr_accessible :photo_ids


  # scope :region, -> (region_id) { where region: region_id }
  # scope :fishes, -> (fish_ids) { where("fish_locations.fish_id IN (?)", fish_ids).joins(:fish_locations) }
  # scope :is_payed, -> (is_payed) { where is_payed: is_payed }
  # scope :title, -> (title) { where("title like ?", "#{title}%")}
  # scope :distance, -> (distance) { where("distance <= ?", "#{distance}%")}

  filterrific(
      # default_filter_params: { sorted_by: 'created_at_desc' },
      available_filters: [
          :title,
          :is_show,
          :with_fish_id,
          :with_region_id,
          :is_payed,
          :with_distance_gte,
          :with_region
      ]
  )

  self.per_page = 20

  scope :title, lambda { |query|
                return nil if query.blank?

                where('title LIKE ?', "%#{query}%")
              }
  scope :sorted_by, lambda { |sort_option|
                    # extract the sort direction from the param value.
                    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
                    case sort_option.to_s
                      when /^created_at_/
                        order("locations.created_at #{ direction }")
                      else
                        raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
                    end
                  }

  scope :with_fish_id, lambda { |fish_ids|
                          where('fish_locations.fish_id IN (?)', [*fish_ids].drop(1)).joins(:fish_locations) if fish_ids.drop(1).size > 0
                        }
  scope :with_region_id, lambda { |region_id|
                              where(:region_id => region_id.drop(1)) if region_id.drop(1).size > 0
                            }

  scope :is_payed, lambda { |is_payed|

                       if is_payed == 1
                         where(:is_payed => !is_payed)
                       else
                         return nil
                       end
                       }

  scope :is_show, lambda { |region_id|
                  where(:is_show => true).uniq
                 }

  scope :with_distance_gte, lambda { |distance|
                   where('distance <= ?', distance)
                 }

  scope :with_region, lambda { |region_id|
                         where(:region_id => region_id, :is_show => true).uniq
                       }


  def self.options_for_select
    order('LOWER(title)').map { |e| [e.title, e.id] }
  end
end
