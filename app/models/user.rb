
class User < ActiveRecord::Base


  # include Mailboxer::Models::Messageable::ActiveRecordExtension

  TEMP_EMAIL_PREFIX = 'change@me'
  TEMP_EMAIL_REGEX = /\Achange@me/

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, :onliner

  # include Mailboxer::Models::Messageable
  acts_as_messageable
  acts_as_commontator
  acts_as_voter

  has_many :articles
  has_many :events
  has_many :locations
  has_many :products
  belongs_to :region
  belongs_to :user_type

  has_attached_file :avatar, :styles => {
                             :detailed => '1920x1920>',
                             :mini => '50x50>',
                             :thumb => '100x100>',
                             :small  => '150x150>',
                             :medium => '200x200>',
                             :to_main => '650x350#',
                             :to_main_profile => '650x650>'
                         }, :default_url => '/images/missing/normal/missing.png'

  # mount :image, ImageUploader

  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  # add a delete_<asset_name> method:
  attr_accessor :delete_asset
  before_validation { self.asset.clear if self.delete_asset == '1' }

  validates :email, :presence => true,
            :length => {:minimum => 3, :maximum => 254},
            :uniqueness => true

  validates :password, :presence => true
  validates :name, :presence => true
  validates :soname, :presence => true
  validates :region_id, :presence => true

  def mailboxer_email(object)
    #Check if an email should be sent for that object
    #if true
    return email
    #if false
    #return nil
  end

  def to_s
    name
  end

  def to_param
    id # or `to_s.parameterize` if you'd like a slug
  end

  def self.find_for_oauth(auth, signed_in_resource = nil)

    # Get the identity and user if they exist
    identity = Identity.find_for_oauth(auth)

    # If a signed_in_resource is provided it always overrides the existing user
    # to prevent the identity being locked with accidentally created accounts.
    # Note that this may leave zombie accounts (with no associated identity) which
    # can be cleaned up at a later date.
    user = signed_in_resource ? signed_in_resource : identity.user

    # Create the user if needed
    if user.nil?

      # Get the existing user by email if the provider gives us a verified email.
      # If no verified email was provided we assign a temporary email and ask the
      # user to verify it on the next step via UsersController.finish_signup
      email_is_verified = auth.info.email && (auth.info.verified || auth.info.verified_email)
      email = auth.info.email if email_is_verified
      user = User.where(:email => email).first if email

      # Create the user if it's a new registration
      if auth.extra.raw_info.name.present?
        u_name = auth.extra.raw_info.name.split(' ')[0]
            u_soname = auth.extra.raw_info.name.split(' ')[1]
      else
        u_name = auth.extra.raw_info.first_name
        u_soname = auth.extra.raw_info.last_name
        avatar_url = auth.extra.raw_info.photo_200_orig

      end
      if user.nil?
        # raise auth.extra.inspect
        user = User.new(
            name: u_name,
            soname: u_soname,
            region_id: 1,
            #username: auth.info.nickname || auth.uid,
            email: email ? email : "#{TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
            password: Devise.friendly_token[0,20]
        )
        # user.skip_confirmation!
        user.save!
      end
    end

    # Associate the identity with the user if needed
    if identity.user != user
      identity.user = user
      identity.save!
    end
    user
  end

  def email_verified?
    self.email && self.email !~ TEMP_EMAIL_REGEX
  end
end
