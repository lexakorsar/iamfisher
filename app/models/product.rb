class Product < ApplicationRecord
  belongs_to :user
  belongs_to :product_category

  has_attached_file :image, :styles => {
      :detailed => '1920x1920>',
      :mini => '50x50>',
      :thumb => '100x100>',
      :small  => '150x150>',
      :medium => '200x200>',
      :to_main => '650x350#',
      :to_main_profile => '650x650>'
  }, :default_url => '/images/missing/normal/missing.png'

  # mount :image, ImageUploader

  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  # add a delete_<asset_name> method:
  attr_accessor :delete_asset
  before_validation { self.asset.clear if self.delete_asset == '1' }
end
