class FishLocation < ActiveRecord::Base
  belongs_to :fish
  belongs_to :location
end
