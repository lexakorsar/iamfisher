class Article < ActiveRecord::Base
  belongs_to :category
  belongs_to :region
  belongs_to :location
  belongs_to :user
  has_many :photos, :dependent => :destroy, :inverse_of => :location
  accepts_nested_attributes_for :photos, :allow_destroy => true

  self.per_page = 10


  acts_as_commontable
  acts_as_votable

  has_attached_file :logo, :styles => {
                              :detailed => '1920x1920>',
                              :thumb => '100x100>',
                              :small  => '150x150>',
                              :medium => '300x300>',
                              :to_main => '650x350#'
                          }

  # mount :image, ImageUploader

  validates_attachment_content_type :logo, :content_type => /\Aimage\/.*\Z/
  # add a delete_<asset_name> method:
  attr_accessor :delete_asset
  before_validation { self.asset.clear if self.delete_asset == '1' }

end
