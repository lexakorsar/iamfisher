class ProductCategory < ApplicationRecord
  has_many :products
  belongs_to :main_product_category

  def self.options_for_select
    order('LOWER(name)').map { |e| [e.name, e.id] }
  end
end
