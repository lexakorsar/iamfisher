class Region < ActiveRecord::Base
  has_many :locations
  has_many :articles
  has_many :users
  belongs_to :province

  def self.options_for_select
    order('LOWER(title)').map { |e| [e.title, e.id] }
  end
end
