class LocationSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :is_payed
  has_many :photos
end
