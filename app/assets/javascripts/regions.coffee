# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$ ->
  $(document).on 'change', '#custom_province_id', (evt) ->
    $.ajax '/update_regions',
      type: 'GET'
      dataType: 'script'
      data: {
        province_id: $("#custom_province_id").val()
      }
      error: (jqXHR, textStatus, errorThrown) ->
      success: (data, textStatus, jqXHR) ->
