CKEDITOR.editorConfig = function (config) {

    config.toolbar_user = [
        ["Bold",  "Italic",  "Underline",  "Strike"],
    ];

    config.path = "/javascripts/"
    config.font_names = 'Open Sans;' + config.font_names;
    //font-family: "Open Sans","Helvetica Neue",Arial,sans-serif;
    config.filebrowserBrowseUrl = "/ckeditor/attachment_files";
    config.filebrowserFlashBrowseUrl = "/ckeditor/attachment_files";
    config.filebrowserFlashUploadUrl = "/ckeditor/attachment_files";
    config.filebrowserImageBrowseLinkUrl = "/ckeditor/pictures";
    config.filebrowserImageBrowseUrl = "/ckeditor/pictures";
    config.filebrowserImageUploadUrl = "/ckeditor/pictures";
    config.filebrowserUploadUrl = "/ckeditor/attachment_files";
    config.extraPlugins = 'spoiler';
}