// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require jquery.easing.min
//= require jquery.fittext
//= require jquery.magnific-popup
//= require select2
//= require select2_locale_ru
//= require dropzone
//= require filterrific/filterrific-jquery
//= require commontator/application
//= require moment
//= require fullcalendar
//= require_tree ./thredded
//= require bootstrap-wysihtml5
//= require bootstrap-wysihtml5/locales
//= require bootstrap-wysihtml5/locales/ru-RU
//= require regions
//= require_self


var ready;
ready = function() {

    $("#filterrific_with_fish_id").select2({
        placeholder: "Обитатели"
    });

    if($("#filterrific_with_region_id")){
        $("#filterrific_with_region_id").select2({
            placeholder: "Регион"
        });
    }

    $('.wysihtml5').each(function(i, elem) {
        $(elem).wysihtml5({ toolbar:{ "fa": true } });
    });

    $('a.page-scroll').bind('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

// Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    })

// Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function () {
        $('.navbar-toggle:visible').click();
    });

// Fit Text Plugin for Main Header
    $("h1").fitText(
        1.2, {
            minFontSize: '35px',
            maxFontSize: '65px'
        }
    );

// Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })

// Initialize WOW.js Scrolling Animations
//    new WOW().init();
}

$(document).ready(ready);
$(document).on('page:load', ready);
