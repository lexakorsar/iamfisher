class CreateEventTypes < ActiveRecord::Migration
  def change
    create_table :event_types do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
    add_reference :events, :event_type, index: true
  end
end
