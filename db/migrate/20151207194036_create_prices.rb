class CreatePrices < ActiveRecord::Migration
  def change
    create_table :prices do |t|
      t.string :type_of_pryce
      t.integer :value_of_price
      t.belongs_to :location, index: true

      t.timestamps null: false
    end
  end
end
