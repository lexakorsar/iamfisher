class AddCompanyFields < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :is_company, :boolean
    add_column :users, :payment_and_delivery, :text
    add_column :users, :wholesalers, :text
    add_column :users, :contacts, :text
    add_column :users, :information_for_clients, :text
    add_column :users, :about_company, :text

    create_table :user_news do |t|
      t.string :title
      t.text :description

      t.timestamps null: false
    end

    # add_reference :events, :user, index: true

  end
end
