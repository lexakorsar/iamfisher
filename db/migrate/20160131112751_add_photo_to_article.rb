class AddPhotoToArticle < ActiveRecord::Migration
  def change
      add_reference :photos, :article, index: true
  end
end
