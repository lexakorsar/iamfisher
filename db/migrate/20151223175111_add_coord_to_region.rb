class AddCoordToRegion < ActiveRecord::Migration
  def change
    add_column :regions, :longt, :float
    add_column :regions, :lat, :float
  end
end
