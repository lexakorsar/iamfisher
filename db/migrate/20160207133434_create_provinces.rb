class CreateProvinces < ActiveRecord::Migration
  def change
    create_table :provinces do |t|
      t.string :title
      t.text :description

      t.timestamps null: false
    end

    add_reference :regions, :province, index: true
  end
end
