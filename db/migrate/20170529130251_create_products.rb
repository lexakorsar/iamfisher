class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.string :price

      t.timestamps
    end

    add_attachment :products, :image

    add_reference :products, :product_category, index: true
    add_reference :products, :user, index: true
  end
end
