class CreateFishLocations < ActiveRecord::Migration
  def change
    create_table :fishes do |t|
      t.string :name
      t.text :description
      t.timestamps null: false
    end

    create_table :fish_locations do |t|
      t.belongs_to :fishes, index: true
      t.belongs_to :locations, index: true
      t.timestamps null: false
    end
  end
end
