class FixUserLocation < ActiveRecord::Migration
  def change
    remove_column :locations, :price, :string
    add_column :locations, :user_price, :string
  end
end
