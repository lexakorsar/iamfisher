class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.date :start
      t.date :end
      t.string :title
      t.text :description

      t.timestamps null: false
    end
  end
end
