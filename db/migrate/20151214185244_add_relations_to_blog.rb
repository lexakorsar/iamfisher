class AddRelationsToBlog < ActiveRecord::Migration
  def change
    add_reference :articles, :user, index: true
    add_reference :articles, :region, index: true
    add_reference :articles, :location, index: true
  end
end
