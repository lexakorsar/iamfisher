class CreatePartners < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :partner_link

      t.timestamps null: false
    end
    add_attachment :partners, :logo
  end
end
