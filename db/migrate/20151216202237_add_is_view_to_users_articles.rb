class AddIsViewToUsersArticles < ActiveRecord::Migration
  def change
    add_column  :locations, :is_show, :boolean, :default => false
    add_column  :articles, :is_show, :boolean, :default => false
  end
end
