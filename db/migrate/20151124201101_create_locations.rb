class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :title
      t.text :description
      t.float :lat
      t.float :longt
      t.text :locations
      t.references :region, index: true
      t.boolean :is_payed,  default: false
      t.integer :day
      t.integer :half_day
      t.integer :night

      t.timestamps null: false
    end
  end
end
