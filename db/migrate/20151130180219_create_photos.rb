class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :caption
      t.belongs_to :location, index: true
      t.boolean :is_main

      t.timestamps null: false
    end
    add_attachment :photos, :image
  end
end
