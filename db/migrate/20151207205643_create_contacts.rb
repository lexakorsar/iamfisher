class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :contact_value
      t.string :contact_description
      t.belongs_to :location, index: true

      t.timestamps null: false
    end
  end
end
