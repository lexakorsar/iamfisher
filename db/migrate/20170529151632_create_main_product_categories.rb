class CreateMainProductCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :main_product_categories do |t|
      t.string :name

      t.timestamps
    end

    add_reference :product_categories, :main_product_category, index: true
  end
end
