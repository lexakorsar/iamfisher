class AddPriceToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :price, :string
    add_column :locations, :location_contact, :string
  end
end
