class AddUserInfo < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :soname, :string
    add_column :users, :phone, :string
    add_column :users, :birthday, :date
    add_column :users, :region_id, :integer
    add_column :users, :sex, :integer
    add_column :users, :description, :text


    add_attachment :users, :avatar
  end
end
