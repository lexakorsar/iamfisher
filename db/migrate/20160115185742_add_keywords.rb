class AddKeywords < ActiveRecord::Migration
  def change
    add_column :locations, :keywords, :text
    add_column :articles, :keywords, :text
  end
end
