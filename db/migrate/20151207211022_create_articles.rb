class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.text :description
      t.belongs_to :category, index: true

      t.timestamps null: false
    end
    add_attachment :articles, :logo
  end
end
