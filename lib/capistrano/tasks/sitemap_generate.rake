namespace :custom do
  desc 'run generate site map'
  task :run_sitemap_generator do
    on roles(:app) do
      within "#{current_path}" do
        with rails_env: "#{fetch(:stage)}" do
          execute :rake, "sitemap:generate"
        end
      end
    end
  end
end