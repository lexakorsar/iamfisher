namespace :custom do
  desc 'run some rake task'
  task :run_paperclip_task do
    on roles(:app) do
      within "#{current_path}" do
        with rails_env: "#{fetch(:stage)}" do
          execute :rake, "paperclip:refresh CLASS=Photo"
        end
      end
    end
  end
end