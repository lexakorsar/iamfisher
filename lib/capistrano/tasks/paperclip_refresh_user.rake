namespace :custom do
  desc 'run some rake task user'
  task :run_paperclip_task_user do
    on roles(:app) do
      within "#{current_path}" do
        with rails_env: "#{fetch(:stage)}" do
          execute :rake, "paperclip:refresh CLASS=User"
        end
      end
    end
  end
end